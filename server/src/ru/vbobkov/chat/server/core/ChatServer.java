package ru.vbobkov.chat.server.core;


import ru.vbobkov.chat.library.Messages;
import ru.vbobkov.chat.network.ServerSocketThread;
import ru.vbobkov.chat.network.ServerSocketThreadListener;
import ru.vbobkov.chat.network.SocketThread;
import ru.vbobkov.chat.network.SocketThreadListener;

import java.net.ServerSocket;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Vector;

public class ChatServer implements ServerSocketThreadListener, SocketThreadListener{

    ServerSocketThread serverSocketThread;
    private final DateFormat dateFormat = new SimpleDateFormat("[hh:mm:ss] ");
    private final ChatServerListener listener;
    Vector<SocketThread> clients = new Vector<>();

    public ChatServer(ChatServerListener listener) {
        this.listener = listener;
    }

    public void start(int port) {
        if (serverSocketThread != null && serverSocketThread.isAlive())
            putLog("Server is already running");
        else {
            serverSocketThread = new ServerSocketThread(this, "Server thread", port, 2000);
            SqlClient.connect();
        }
    }

    public void stop() {
        if (serverSocketThread == null || !serverSocketThread.isAlive())
            putLog("Server is not running");
        else {
            serverSocketThread.interrupt();
            SqlClient.disconnect();
        }
    }

    void putLog(String msg) {
        msg = dateFormat.format(System.currentTimeMillis()) + Thread.currentThread().getName() + ": " + msg;
        listener.onChatServerLog(this, msg);
    }

    private String getUsers() {
        // u1§u2§u3§
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < clients.size(); i++) {
            ClientThread client = (ClientThread) clients.get(i);
            if (!client.isAuthorized()) continue;
            stringBuilder.append(client.qetNickname() + Messages.DELIMITER);
        }
        return stringBuilder.toString();
    }

    private ClientThread findClientByNickname (String nickname) {
        for (int i = 0; i < clients.size(); i++) {
            ClientThread client = (ClientThread) clients.get(i);
            if (!client.isAuthorized()) continue;
            if (nickname.equals(client.qetNickname()))
                return client;
        }
        return null;
    }

    // ===== implements methods ServerSocketThreadListener =====
    @Override
    public void onStartServerSocketThread(ServerSocketThread thread) {
        putLog("Сервер запущен");
    }

    @Override
    public void onStopServerSocketThread(ServerSocketThread thread) {
        putLog("Сервер остановлен");
    }

    @Override
    public void onCreateServerSocket(ServerSocketThread thread, ServerSocket serverSocket) {
        putLog("ServerSocket создан");
    }

    @Override
    public void onAcceptTimeout(ServerSocketThread thread, ServerSocket serverSocket) {

    }

    @Override
    public void onSocketAccepted(ServerSocketThread thread, Socket socket) {
        putLog("Клиент подключился: " + socket);
        String threadName = "SocketThread " + socket.getInetAddress() + ":" + socket.getPort();
        new ClientThread(this, threadName, socket);
    }

    @Override
    public void onServerSocketException(ServerSocketThread thread, Exception e) {
        putLog("Exception " + e.getClass().getName() + ": " + e.getMessage());
    }

    // ===== implements methods SocketThreadListener =====
    @Override
    public synchronized void onStartSocketThread(SocketThread thread, Socket socket) {
        putLog("Socket стартовал");
    }

    @Override
    public synchronized void onStopSocketThread(SocketThread thread) {
        putLog("Socket остановлен");
        ClientThread client = (ClientThread) thread;
        clients.remove(thread);
        if (client.isAuthorized()) {
            thread.sendMessage(Messages.getTypeBroadcast("Server", client.qetNickname() + " отключился"));
            sendToAuthorizedClients(Messages.getUserList(getUsers()));
        }
    }

    @Override
    public synchronized void onSocketIsReady(SocketThread thread, Socket socket) {
        putLog("Socket готов");
        clients.add(thread);
    }

    @Override
    public synchronized void onReceiveString(SocketThread thread, Socket socket, String value) {
        ClientThread client = (ClientThread) thread;
        if (client.isAuthorized())
            handleAuthMessages(client, value);
        else
            handleNonAuthMessages(client, value);
    }

    @Override
    public synchronized void onSocketThreadException(SocketThread thread, Exception e) {
        putLog("Exception " + e.getClass().getName() + ": " + e.getMessage());
    }

    private void handleNonAuthMessages(ClientThread newClient, String value){
        String[] arr = value.split(Messages.DELIMITER);
        if (arr.length != 3 || !arr[0].equals(Messages.AUTH_REQUEST)) {
            newClient.msgFormatError(value);
            return;
        }
        String login = arr[1];
        String password = arr[2];
        String nickname = SqlClient.getNick(login, password);
        if (nickname == null) {
            putLog("Invalid login/password: login '" +
                    login + "' password: '" + password + "'");
            newClient.authorizeError();
            return;
        }

        ClientThread client = findClientByNickname(nickname);
        newClient.authorizeAccept(nickname);
        if (client == null) {
            sendToAuthorizedClients(Messages.getTypeBroadcast("Server", "Пользователь " + nickname + " присоединился"));
        }
        else {
            client.reconnect();
            clients.remove(client);
        }
        sendToAuthorizedClients(Messages.getUserList(getUsers()));

    }

    private void handleAuthMessages(ClientThread client, String value) {
        String[] arr = value.split(Messages.DELIMITER);
        String msgType = arr[0];
        switch (msgType) {
            case Messages.TYPE_RANGECAST:
                sendToAuthorizedClients(Messages.getTypeBroadcast(client.qetNickname(), arr[1]));
                break;
            default:
                client.msgFormatError(value);
        }
    }

    private void sendToAuthorizedClients(String value) {
        for (int i = 0; i < clients.size(); i++) {
            ClientThread client = (ClientThread) clients.get(i);
            if (!client.isAuthorized()) continue;
            client.sendMessage(value);
        }
    }
}
