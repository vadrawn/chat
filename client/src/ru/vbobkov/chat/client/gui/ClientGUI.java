package ru.vbobkov.chat.client.gui;

import com.sun.org.apache.xalan.internal.xsltc.compiler.util.StringStack;
import ru.vbobkov.chat.library.Messages;
import ru.vbobkov.chat.network.SocketThread;
import ru.vbobkov.chat.network.SocketThreadListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;

public class ClientGUI extends JFrame implements Thread.UncaughtExceptionHandler, ActionListener, SocketThreadListener {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ClientGUI();
            }
        });
    }

    private final int WIDTH = 500;
    private final int HEIGHT = 400;

    private static String WINDOW_TITLE = "Chat Client";
    private final DateFormat dateFormat = new SimpleDateFormat("[hh:mm:ss] ");


    File fLog = new File("src/server/server.log");

    // Panels
    JPanel panelTop = new JPanel(new GridLayout(2,3));
    JPanel panelBottom = new JPanel(new BorderLayout());

    // Top fields
    JTextField tfIP = new JTextField("127.0.0.1"); //95.84.209.91
    JTextField tfPort = new JTextField("8189");
    JCheckBox cbAlwaysOnTop = new JCheckBox("Always on top");
    JTextField tfLogin = new JTextField("login");
    JPasswordField tfPassword = new JPasswordField("123456");
    JButton btnLogin = new JButton("login");

    // Center fields
    JTextArea log = new JTextArea("Welcome to the chat!" + '\n');
    JList<String> userList = new JList<>();

    // Bottom fields
    JButton btnDisconnect = new JButton("Disconnect");
    JTextField tfMessage = new JTextField();
    JButton btnSend = new JButton("Send");

    SocketThread socketThread;

    ClientGUI() {
        Thread.setDefaultUncaughtExceptionHandler(this);
        setTitle(WINDOW_TITLE);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(WIDTH, HEIGHT);

        // --- Top ---
        tfIP.addActionListener(this);
        tfPort.addActionListener(this);
        cbAlwaysOnTop.addActionListener(this);
        tfLogin.addActionListener(this);
        tfPassword.addActionListener(this);
        btnLogin.addActionListener(this);

        panelTop.add(tfIP);
        panelTop.add(tfPort);
        panelTop.add(cbAlwaysOnTop);
        panelTop.add(tfLogin);
        panelTop.add(tfPassword);
        panelTop.add(btnLogin);
        add(panelTop, BorderLayout.NORTH);

        // --- Center ---
        // log area
        log.setEditable(false);
        log.setLineWrap(true);
        JScrollPane scrollLog = new JScrollPane(log);
        add(scrollLog, BorderLayout.CENTER);
        // users area
        //userList.setListData(users);
        JScrollPane scrollUsers = new JScrollPane(userList);
        scrollUsers.setPreferredSize(new Dimension(100,0));
        add(scrollUsers, BorderLayout.EAST);

        // --- Bottom ---
        // Disconnect button
        btnDisconnect.addActionListener(this);
        panelBottom.add(btnDisconnect, BorderLayout.WEST);
        // message field
        tfMessage.addActionListener(this);
        panelBottom.add(tfMessage, BorderLayout.CENTER);
        // Send button
        btnSend.addActionListener(this);
        panelBottom.add(btnSend, BorderLayout.EAST);
        // Panel bottom
        add(panelBottom, BorderLayout.SOUTH);
        panelBottom.setVisible(false);

        setLocationRelativeTo(null);
        setVisible(true);
    }

    // ===== implements ActionListener =====
    @Override
    public void actionPerformed(ActionEvent e) {
        Object src = e.getSource();
        if (src == cbAlwaysOnTop)
            setAlwaysOnTop(cbAlwaysOnTop.isSelected());
        else if (src == btnSend || src == tfMessage)
            sendMsg(tfMessage.getText());
        else if (src == tfIP || src == tfPort ||
                 src == tfLogin || src == tfPassword || src == btnLogin) {
            connect();
        }
        else if (src == btnDisconnect)
            disconnect();
        else
            throw new RuntimeException("Unexpected source: " + src);
    }

    private void connect() {
        Socket socket = null;
        try {
            socket = new Socket(tfIP.getText(), Integer.parseInt(tfPort.getText()));
        } catch (IOException e) {
            putLog("Exception: " + e.getMessage());
            return;
        }
        socketThread = new SocketThread(this, "SocketThread", socket);
    }
    private void disconnect() {
        socketThread.close();
    }

    void sendMsg(String text) {
        if (text.equals("")) return;
        tfMessage.setText(null);
        tfMessage.requestFocusInWindow();
        socketThread.sendMessage(Messages.getTypeRangecast(text));
    }

    private void putLog(String msg) {
        log.append(msg + '\n');
        log.setCaretPosition(log.getDocument().getLength());
    }


    // ===== implements SocketThreadListener =====
    @Override
    public void onStartSocketThread(SocketThread thread, Socket socket) {
        putLog("Поток сокета стартовал");
    }

    @Override
    public void onStopSocketThread(SocketThread thread) {
        putLog("Соединение разорвано");
        setTitle(WINDOW_TITLE);
        userList.setListData(new String[0]);
        panelBottom.setVisible(false);
        panelTop.setVisible(true);
    }

    @Override
    public void onSocketIsReady(SocketThread thread, Socket socket) {
        putLog("Соединение установлено");
        String login = tfLogin.getText();
        String pass = new String(tfPassword.getPassword());
        thread.sendMessage(Messages.getAuthRequest(login,pass));
        panelBottom.setVisible(true);
        panelTop.setVisible(false);
        tfMessage.requestFocusInWindow();
    }

    @Override
    public void onReceiveString(SocketThread thread, Socket socket, String value) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                String[] arr = value.split(Messages.DELIMITER);
                switch (arr[0]) {
                    case Messages.AUTH_ACCEPT:
                        putLog("Вы вошли под именем " + arr[1]);
                        setTitle(WINDOW_TITLE + ": " + arr[1]);
                        break;
                    case Messages.TYPE_BROADCAST:
                        putLog(dateFormat.format(Long.parseLong(arr[1])) + arr[2] + ": " + arr[3]);
                        break;
                    case Messages.USER_LIST:
                        String users = value.substring(Messages.USER_LIST.length() + Messages.DELIMITER.length());
                        String[] userArray = users.split(Messages.DELIMITER);
                        Arrays.sort(userArray);
                        userList.setListData(userArray);
                        break;
                    case Messages.AUTH_DENIED:
                        putLog("Invalid login/password...");
                        break;
                    case Messages.MSG_FORMAT_ERROR:
                        putLog("Неправильный формат сообщения...");
                        socketThread.close();
                        break;
                    default:
                        throw new RuntimeException("Unknown message format: " + value);
                }
            }
        });
    }

    @Override
    public void onSocketThreadException(SocketThread thread, Exception e) {

    }

    // ===== implements Thread.UncaughtExceptionHandler =====
    @Override
    public void uncaughtException(Thread t, Throwable e) {
        e.printStackTrace();
        StackTraceElement[] stackTraceElements = e.getStackTrace();
        String msg;
        if (stackTraceElements.length == 0) {
            msg = "Empty Stacktrace";
        }
        else {
            msg = e.getClass().getCanonicalName() + ":" + e.getMessage() + "\n" +
                    "\t at" + stackTraceElements[0];
        }
        JOptionPane.showMessageDialog(this, msg);
        System.exit(1);
    }

//    private void addToLogFile(String text) {
//        // try-with-resources
//        try (FileWriter fw = new FileWriter(fLog, true)) {
//            fw.write(text);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
}
